// minimal.cpp: Display the landmarks of a face in an image.
//              This demonstrates stasm_search_single.

#include <stdio.h>
#include <stdlib.h>
#include "opencv2/core/core.hpp"
#include "opencv/highgui.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "stasm_lib.h"

int minimal(const cv::Mat& gray, const char* const path)
{
	cv::Mat_<unsigned char> img(gray);

	if (!img.data)
	{
		printf("Cannot load %s\n", path);
		exit(1);
	}

	int foundface;
	float landmarks[2 * stasm_NLANDMARKS]; // x,y coords (note the 2)

	if (!stasm_search_single(&foundface, landmarks,
		(const char*)img.data, img.cols, img.rows, path, "../../data"))
	{
		printf("Error in stasm_search_single: %s\n", stasm_lasterr());
		exit(1);
	}

	if (!foundface)
		printf("No face found in %s\n", path);
	else
	{
		// draw the landmarks on the image as white dots (image is monochrome)
		stasm_force_points_into_image(landmarks, img.cols, img.rows);
		for (int i = 0; i < stasm_NLANDMARKS; i++)
			img(cvRound(landmarks[i * 2 + 1]), cvRound(landmarks[i * 2])) = 255;
	}

	cv::imwrite("minimal.bmp", img);
	cv::imshow("stasm minimal", img);
	return 0;
}

int video()
{
	cv::VideoCapture cap(CV_CAP_ANY);
	if (!cap.isOpened())
	{
		printf("!cap.isOpened()\n");
		return -1;
	}

	cv::Mat frame;
	cv::Mat gray;
	while (true)
	{
		cap >> frame;
		if (frame.empty())
			continue;

		cv::imshow("video capture", frame);

		cvtColor(frame, gray, CV_BGR2GRAY);
		minimal(gray, "video capture");


		if (cv::waitKey(20) >= 0)
			break;
	}
	return 0;
}

int main1()
{
    static const char* const path = "../../data/testface.jpg";
	const cv::Mat& gray = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);
	minimal(gray, path);
    cv::waitKey();
    return 0;
}

int main2()
{
	video();
	return 0;
}
int main()
{
	main2();
	return 0;
}
