// landtab_hands.h: definitions for imm403 56 point hand shapes
//
// Copyright (C) 2005-2013, Stephen Milborrow

#ifndef STASM_LANDTAB_AFLW21_H
#define STASM_LANDTAB_AFLW21_H

// Note that we use classical 1D descriptors on all points (no HATS)
// because all points are on edges.  HATs would probably work as well
// but I haven't tried them here (to use a HAT descriptor at a point,
// set the AT_Hat bit in the "bits" field).

static const LANDMARK_INFO LANDMARK_INFO_TAB[39] = // 39 points
{ // par pre next weight bits
    { -1, -1,  -1,     1,   0 }, // 00
    { -1, -1,  -1,     1,   0 }, // 01
    { -1, -1,  -1,     1,   0 }, // 02
    { -1, -1,  -1,     1,   0 }, // 03
    { -1, -1,  -1,     1,   0 }, // 04
    { -1, -1,  -1,     1,   0 }, // 05
    { -1, -1,  -1,     1,   0 }, // 06
    { -1, -1,  -1,     1,   0 }, // 07
    { -1, -1,  -1,     1,   0 }, // 08
    { -1, -1,  -1,     1,   0 }, // 09
    { -1, -1,  -1,     1,   0 }, // 10
    { -1, -1,  -1,     1,   0 }, // 11
    { -1, -1,  -1,     1,   0 }, // 12
    { -1, -1,  -1,     1,   0 }, // 13
    { -1, -1,  -1,     1,   0 }, // 14
    { -1, -1,  -1,     1,   0 }, // 15
    { -1, -1,  -1,     1,   0 }, // 16
    { -1, -1,  -1,     1,   0 }, // 17
    { -1, -1,  -1,     1,   0 }, // 18
    { -1, -1,  -1,     1,   0 }, // 19
    { -1, -1,  -1,     1,   0 }, // 20
    { -1, -1,  -1,     1,   0 }, // 21
    { -1, -1,  -1,     1,   0 }, // 22
    { -1, -1,  -1,     1,   0 }, // 23
    { -1, -1,  -1,     1,   0 }, // 24
    { -1, -1,  -1,     1,   0 }, // 25
    { -1, -1,  -1,     1,   0 }, // 26
    { -1, -1,  -1,     1,   0 }, // 27
    { -1, -1,  -1,     1,   0 }, // 28
    { -1, -1,  -1,     1,   0 }, // 29
    { -1, -1,  -1,     1,   0 }, // 30
    { -1, -1,  -1,     1,   0 }, // 31
    { -1, -1,  -1,     1,   0 }, // 32
    { -1, -1,  -1,     1,   0 }, // 33
    { -1, -1,  -1,     1,   0 }, // 34
    { -1, -1,  -1,     1,   0 }, // 35
    { -1, -1,  -1,     1,   0 }, // 36
    { -1, -1,  -1,     1,   0 }, // 37
    { -1, -1,  -1,     1,   0 }, // 38
};

#endif // STASM_LANDTAB_AFLW21_H
